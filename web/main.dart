library couclockwidget;

import "dart:async";
import "dart:convert";
import "dart:html";

part "package:couclockwidget/clock.dart";
part "package:couclockwidget/dom.dart";

void main() {
	final Element CLOCK_ELEMENT = querySelector("#couclockwidget");
	final Map<String, SpanElement> ELEMENTS = assembleDom(CLOCK_ELEMENT);

	clock.onUpdate.listen((List data) {
		CLOCK_ELEMENT.attributes["loaded"] = "true";

		ELEMENTS["time"].text = data[0];
		ELEMENTS["dayNum"].text = data[1];
		ELEMENTS["dayName"].text = data[2];
		ELEMENTS["month"].text = data[3];
		ELEMENTS["year"].text = data[4];
	});

	clock._sendEvents();
}
