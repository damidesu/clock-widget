part of couclockwidget;

Map<String, Element> assembleDom(Element parentElement) {
	DivElement clockParent = new DivElement()
		..classes.add("coucw");

	DivElement time = new DivElement()
		..classes.add("coucw-time");

	SpanElement dayNum = new SpanElement()
		..classes.add("coucw-date-daynum");

	SpanElement dayName = new SpanElement()
		..classes.add("coucw-date-dayname");

	SpanElement month = new SpanElement()
		..classes.add("coucw-date-month");

	DivElement date = new DivElement()
		..classes.add("coucw-date")
		..append(dayName)
		..appendText(", ")
		..append(dayNum)
		..appendText(" of ")
		..append(month);

	DivElement year = new DivElement()
		..classes.add("coucw-year");

	clockParent
		..append(time)
		..append(date)
		..append(year);

	parentElement.append(clockParent);

	return ({
		"parent": clockParent,
		"time": time,
		"dayNum": dayNum,
		"dayName": dayName,
		"month": month,
		"year": year
	});
}